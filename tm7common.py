################################################################################
## Defines TM7 common functions
################################################################################

import uhalaux
import time
from bcolors import bcolors

def read(self, reg, mask = 0xFFFFFFFF, nodispatch = False, fresh = True, storeSuffix = ''):
  """IPBus read operation in register "reg" of TM7, which may be a textual register or an address.
  Optional argument mask is only used with a numerical address"""
  reg_suffixed = str(reg) + storeSuffix
  if fresh or not reg_suffixed in self.storedReads:
    self.storedReads[reg_suffixed] = uhalaux.read(self.hw(), reg, mask, nodispatch)
  return self.storedReads[reg_suffixed]
  
def write(self, reg, data, mask = 0xFFFFFFFF):
  """IPBus write operation in register "reg" of TM7, which may be a textual register or an address.
  Optional argument mask is only used with a numerical address"""
  return uhalaux.write(self.hw(), reg, data, mask)

def getNode(self, reg):
  return self.hw().getNode(reg)
  
def reg_num2name(self, regnum, mask = 0xFFFFFFFF):
  return uhalaux.reg_num2name(self.hw(), regnum, mask)
  
def gth_debuginfo(self):
  stat9 = self.read('readout.stat.gth')
  print('rxrstdone',stat9&0x1,)
  print('txrstdone',(stat9>>1)%0x1,)
  print('datavalid',(stat9>>2)%0x1,)
  print('rxrstdbg',(stat9>>3)%0x1,)
  print( 'cplllock',(stat9>>4)%0x1,)
  print( 'txrstdbg',(stat9>>5)%0x1,)
  print('errctr',(stat9>>8)%0xFFFF)

def amc13link_reset_duration(self, val=None):
   """Queries (val=None) or sets de amc13link reset duration register.
   0 <= val <= 15: the duration is 2**val-1 in ipbus clock cycles"""
   if val==None: print(self.read('readout.conf.amc13.rst_len'))
   elif val in range(16): self.write('readout.conf.amc13.rst_len',val)
   else: print("val must be None or an integer from 0 to 15")

#MP7_TTC module
def ttc_err_ctr_clear(self):
  self.write('ttc.csr.ctrl.err_ctr_clear',1)
  self.write('ttc.csr.ctrl.err_ctr_clear',0)
def ttc_ctr_clear(self):
  self.write('ttc.csr.ctrl.ctr_clear',1)
  self.write('ttc.csr.ctrl.ctr_clear',0)

def measfreq(self, clk = 'lhc', timeout=2):
  """Returns the frequency measurement from the TTC counter module for the specified clock (default is 'lhc' clock)."""
  aliases = {'lhc':0}
  if clk in aliases:
    clk = aliases[clk]
  if not clk in range(3):
    return 'wrong clock identifier'
  if self.read('ttc.freq.ctrl.chan_sel') != clk:
    self.write('ttc.freq.ctrl.chan_sel',clk)
    t0 = time.time()
    while (time.time < t0 + timeout) and not self.read('ttc.freq.freq.valid'):
      time.sleep(.1)
  if self.read('ttc.freq.freq.valid'):
    return 64 * 31.25 * self.read('ttc.freq.freq.count') / (2**24)
  else:
    return 'freq reading returned invalid'

def ttc_stats(self):
  """Prints the status registers from TTC module."""
  results = []
  maxlength = 0
  for node in self.hw().getNodes('ttc\.csr\.stat.\..*'):
    nodeid = node.split('.')[-1]
    results+=[(nodeid,self.read(node))]
    maxlength = max(maxlength, len(nodeid))
  for res in results:
    print(res[0]+' '*(maxlength-len(res[0])), hex(res[1]))

def verifyBoardTypeVersion(self, params, indent=''):
  b_id = self.read('ctrl.id.fwrev.design')
  if b_id != 0xD7: print (bcolors.red + 'board_id = 0x%02X != 0xD7'%b_id + bcolors.reset)
  algorev = self.read('ctrl.id.algorev')
  boardType, FWvers = (algorev>>24)&0xFF, algorev&0xFFFFFF
  if boardType != self.fw_type_identifier:
    print( indent + bcolors.red + 'board id=%s does not self-identify as %s-type FW (0x%02X): algorev = 0x%02X'%(self.hw().id(), self.__class__.__name__, self.fw_type_identifier, boardType) + bcolors.reset)
  if FWvers != params['fw_version']:
    print( indent + bcolors.red + 'board id=%s fw version (%i) different than expected (%i)'%(self.hw().id(), FWvers, params['fw_version']) + bcolors.reset)
  return FWvers
