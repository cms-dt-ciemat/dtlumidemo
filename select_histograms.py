import glob
import os
from datetime import datetime
import numpy as np
import parse
from analysis import *
from matplotlib import pyplot as plt

METHOD = 2

def get_date(mtime):
    return datetime.fromtimestamp(mtime).strftime('%Y%m%d_%H%M%S')

def load_dt_data(start_time = 0):
    dt_start_time = -1
    dt_end_time = -1
    dt_hist = np.zeros(3564, dtype="int")

    #dt_file = open("./from_cristina/out340854_qlg4.txt", "r")
    #dt_file = open("./from_cristina/file341800.txt", "r")
    #dt_file = open("./from_cristina/removedfirstsfile341800.txt", "r")
    dt_file = open("./from_cristina/343442_allinfo_stripped.txt", "r")
    lines = dt_file.readlines()
    dt_file.close()

    line_id = 0
    while(line_id < len(lines)):
        if lines[line_id][:9] == "timestamp":
            time_str = lines[line_id][:-1].split("=")[1]
            if dt_start_time == -1:
                dt_start_time = datetime.strptime(time_str, "%Y%m%d_%H%M%S").timestamp()
            dt_end_time = datetime.strptime(time_str, "%Y%m%d_%H%M%S").timestamp()
            line_id += 1 # next line
            bx_dict = {}
            if METHOD == 1:
                line_id += 2 # skip bx id and num tpgs lines
                while(lines[line_id][:5] == "evnum"):
                    orbit, q, t0, bxmy, bx, bx_offset = parse.parse("  orbit={:d} q={:d}, t0={:d}, bxmy={:d} bx={:d}, bx_offset={:d}\n", lines[line_id])
                    #print(q, t0, bx, bx_offset)
                    bx_corrected = (3564 + bx - bx_offset - 40) % 3564
                    if bx_corrected in bx_dict.keys():
                        bx_dict[bx_corrected] = bx_dict[bx_corrected] + 1
                    else:
                        bx_dict[bx_corrected] = 1
                    line_id += 1
            if METHOD == 2:
                line_id += 1 # skip event id line
                while(lines[line_id][:19] == "BXid of the event= "):
                    bx, = parse.parse("BXid of the event= {:d}\n", lines[line_id])
                    bx_offset = 0
                    # print(q, t0, bx, bx_offset)
                    bx_corrected = (3564 + bx - bx_offset - 40) % 3564
                    if bx_corrected in bx_dict.keys():
                        bx_dict[bx_corrected] = bx_dict[bx_corrected] + 1
                    else:
                        bx_dict[bx_corrected] = 1
                    line_id += 1
            for key in bx_dict.keys():
                dt_hist[key] += 1
        else:
            line_id += 1

    print("DT: ", get_date(dt_start_time), " - ", get_date(dt_end_time))
    return dt_start_time, dt_end_time, dt_hist
    #print(dt_hist)

def load_bril_data(dt_start_time, dt_end_time):
    files = glob.glob("./stored_histograms/histo*.txt")
    files.sort(key=os.path.getmtime)
    time_prev = 0
    time_offset = 0
    hist_start_time = -1
    for file in files:
        if os.stat(file).st_mtime < dt_start_time + time_offset or (os.stat(file).st_mtime >= dt_start_time+time_offset and hist_start_time == -1):
            hist_start_time = os.stat(file).st_mtime
            hist_start_file = file
        if time_prev < dt_end_time + time_offset:
            hist_end_time = os.stat(file).st_mtime
            hist_end_file = file
        if time_prev == 0:
            print("bril start time", os.stat(file).st_mtime)
        time_prev = os.stat(file).st_mtime
    print("BRIL: ", get_date(hist_start_time), " - ", get_date(hist_end_time), " totalling ", len(files[files.index(hist_start_file):files.index(hist_end_file)+1]))
    print("\t", hist_start_file, " - ", hist_end_file)
    return files[files.index(hist_start_file):files.index(hist_end_file)+1]

dt_start_time, dt_end_time, dt_hist = load_dt_data()
print("dt start time:", dt_start_time)
print("dt emd time:", dt_end_time)
selected_bril_histograms = load_bril_data(dt_start_time, dt_end_time)
bril_hist = parse_list(selected_bril_histograms)

print("DT HIST:")
for bin in range(nbins):
    if dt_hist[bin]:
        print(bin, " - ", dt_hist[bin])
print("BRIL HIST:")
for bin in range(nbins):
    if bril_hist[bin]:
        print(bin, " - ", bril_hist[bin])

plt.subplot(211 + 0)
plt.plot(np.arange(len(dt_hist)), dt_hist)
plt.title("dt")

plt.subplot(211 + 1)
plt.plot(np.arange(len(dt_hist)), bril_hist)
plt.title("bril")

count = 0
for bin in range(nbins):
    if dt_hist[bin] != bril_hist[bin]:
        print("Bin ", bin, " does not match, DT: ", dt_hist[bin], ", BRIL: ", bril_hist[bin])
        count += 1

print("Total mismatch", count)

plt.show()
