## Running the software
- Clone the repository: ```git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/cms-dt-phase2/dtlumidemo.git && cd dtlumidemo```
- Install pybind11: ```pip2 install pybind11```
- Compile the BrilHistogram decoder: ```cd ./bril_histogram/components/bril_histogram/software && make py2 && cd -```
- Python interactive: ```python2 -i lumi.py``` and ```Test()```
