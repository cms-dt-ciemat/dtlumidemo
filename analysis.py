# sys unclude
import numpy as np
import glob
import matplotlib.pyplot as plt
from time import sleep

# pybindings for the bril histo decoder
import sys, os
sys.path.append(os.path.abspath('./bril_histogram/components/bril_histogram/software/lib/'))
from bril_histogram_pybindings import BrilHistogramError
from bril_histogram_pybindings import BrilHistogramManual
# print method
from lumi import histogram_str

# define
nbins = 3564

def parse_histogram(filename):
    file = open(filename,'r')
    line = file.readline()
    # some default
    counts = np.zeros(nbins, dtype="int")
    mask = []
    errors = []
    while (line):
        # parse line
        if "LHC Fill: " in line:
            lhc_fill = int(line.split(":")[1])
        elif "CMS Run: " in line:
            cms_run = int(line.split(":")[1])
        elif "Lumi Section: " in line:
            lumi_section = int(line.split(":")[1])
        elif "Lumi Nibble: " in line:
            lumi_nibble = int(line.split(":")[1])

        elif "Total words: " in line:
            total_nwords = int(line.split(":")[1])
        elif "Header size: " in line:
            header_size = int(line.split(":")[1])
        elif "Number of counter words: " in line:
            ncounter_words = int(line.split(":")[1])
        elif "Number of mask and error words: " in line:
            number_of_mask_error_words = int(line.split(":")[1])
        elif "Number of word per error: " in line:
            number_of_words_per_error = int(line.split(":")[1])
        elif "Histogram id: " in line:
            histogram_id = int(line.split(":")[1])
        elif "Histogram type: " in line:
            histogram_type = int(line.split(":")[1])
        elif "Increment width: " in line:
            increment_width = int(line.split(":")[1])
        elif "Counter width: " in line:
            counter_width = int(line.split(":")[1])
        elif "Orbit counter: " in line:
            orbit_counter = int(line.split(":")[1])
        elif "Counter overflow: " in line:
            counter_ovf = (line.split()[-1]) == "True"
        elif "Increment overflow: " in line:
            increment_ovf = (line.split()[-1]) == "True"
        elif "Number of units: " in line:
            number_of_units = int(line.split(":")[1])
        elif "Non-zero counter bins:" in line:
            # skip table header
            line = file.readline()
            # now read first bin word
            line = file.readline()
            # iterate all bins
            while(len(line.split()) > 0):
                bin_id = int(line.split()[0])
                count = int(line.split()[1])
                counts[bin_id] = count
                line = file.readline()
        elif "Unit mask:" in line:
            # skip table header
            line = file.readline()
            # now read first module word
            line = file.readline()
            # iterate all bins
            while(len(line.split()) > 0):
                module_id = int(line.split()[0])
                mask.append(module_id)
                line = file.readline()
        elif "Errors:" in line:
            print("Found errors! Don't have a recipe to parse them")
            sys.exit(1)

        # read next line
        line = file.readline()

    # close file
    file.close()

    # return
    return BrilHistogramManual(total_nwords, header_size, nbins, histogram_id, histogram_type, ncounter_words, increment_width,
                        counter_width, orbit_counter, increment_ovf, counter_ovf, lhc_fill, cms_run, lumi_section,
                        lumi_nibble, number_of_mask_error_words, number_of_units, number_of_words_per_error, counts,
                        mask, errors)

def parse_list(filelist):
    # get histograms
    histograms = []
    for filename in filelist:
        histograms.append(parse_histogram(filename))

    # combine them
    bins = np.arange(nbins)
    counts = np.zeros(nbins, dtype="int")
    for histogram in histograms:
        histo_counts = histogram.GetCounters()
        for bin in bins:
            counts[bin] += histo_counts[bin]

    # get some stats
    total = 0
    for count in counts:
        total += count
    average = total / nbins
    std_dev = 0
    for count in counts:
        std_dev = (count - average) * (count - average)
    std_dev /= nbins
    std_dev = np.sqrt(std_dev)
    print("\tTotal counts for all bins: ", total, " [counts]")
    print("\tAverage count across bins: ", average / len(histograms), " [counts per bin per histogram], std dev: ",
          std_dev / len(histograms))
    print("\tRate: ", total / len(histograms) / (nbins * (2 ** 12) / 40.078e6), " [Hz]")

    return counts

def parse_run(foldername):
    filelist = glob.glob(foldername + "/histo_*.txt")
    return parse_list(filelist)


def process_all():
    runfolders = glob.glob("./stored_histograms*")
    id = 0
    offset = 0
    for foldername in runfolders:
        if id < offset:
            id += 1
            continue
        print("Run: ", foldername)
        hist = parse_run(foldername)

        if len(runfolders) > 1:
            division = 301
            division += int(len(runfolders)/3) + 1
            plt.subplot(division + id - offset)
        plt.plot(np.arange(len(hist)), hist)
        plt.title(foldername)
        id += 1

    plt.show()