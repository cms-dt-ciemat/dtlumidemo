import tm7common
#from uhal._core import HwInterface

class AB7_minimized():
  read                      = tm7common.read
  write                     = tm7common.write
  getNode                   = tm7common.getNode
  
  def hw(self):
    return self.device
  
  def __init__(self, device):
    self.storedReads = {}
    self.device = device

  def dispatch(self):
    self.hw().dispatch()

  def readBlock(self, reg_name, size):
    # read the value
    reg = self.hw().getNode(reg_name).readBlock(size)
    # transaction
    self.hw().dispatch()
    # return
    return reg.value()
    
