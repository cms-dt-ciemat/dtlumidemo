#!/bin/env python

# basic includes
import uhal
from ab7_minimized import AB7_minimized
from bcolors import bcolors
from pprint import pprint
import time
from time import sleep
import queue
import threading

# add bril histogram
import sys, os
sys.path.append(os.path.abspath('./bril_histogram/components/bril_histogram/software/lib/'))
from bril_histogram_pybindings import BrilHistogramError
from bril_histogram_pybindings import BrilHistogram

# init board
uhal.setLogLevelTo(uhal.LogLevel.WARNING)
#dev = uhal.getDevice('device_id','chtcp-2.0://bridge-s1d15-38:10203?target=amc-s1d15-38-09:50001', 'file://addrtab/ab7_infra.xml')
#dev = uhal.getDevice('devicsoure_id','chtcp-2.0://localhost:65203?target=10.176.131.188:50001', 'file://addrtab/ab7_infra.xml') # amc-s1d15-38-09
#dev = uhal.getDevice('device_id','chtcp-2.0://localhost:65203?target=10.176.131.187:50001', 'file://addrtab/ab7_infra.xml') # amc-s1d15-38-08
#dev = uhal.getDevice('device_id','chtcp-2.0://localhost:65203?target=10.176.131.190:50001', 'file://addrtab/ab7_infra.xml') # amc-s1d15-38-11
#dev = uhal.getDevice('device_id','chtcp-2.0://localhost:65203?target=10.176.131.189:50001', 'file://addrtab/ab7_infra.xml') # amc-s1d15-38-10
dev = uhal.getDevice('device_id','chtcp-2.0://localhost:65203?target=10.176.131.186:50001', 'file://addrtab/ab7_infra.xml') # amc-s1d15-38-07
board = AB7_minimized( dev )

# print histogram method
def histogram_str(histogram, doPrint = True):
    header = "HISTOGRAM PRINTOUT"

    header += "\nHeader:"

    header += "\n\tTCDS data:"
    header += "\n\t\tLHC Fill: " + str(histogram.GetLhcFill())
    header += "\n\t\tCMS Run: " + str(histogram.GetCmsRun())
    header += "\n\t\tLumi Section: " + str(histogram.GetLumiSection())
    header += "\n\t\tLumi Nibble: " + str(histogram.GetLumiNibble())

    header += "\n\tHistogram header:"
    header += "\n\t\tTotal words: " + str(histogram.GetNwordsTotal())
    header += "\n\t\tHeader size: " + str(histogram.GetHeaderSize())
    header += "\n\t\tNumber of counter words: " + str(histogram.GetNcounterWords())
    header += "\n\t\tNumber of mask and error words: " + str(histogram.GetNmaskErrorWords())
    header += "\n\t\tNumber of word per error: " + str(histogram.GetNwordsPerError())
    header += "\n\t\tHistogram id: " + str(histogram.GetHistogramID())
    header += "\n\t\tHistogram type: " + str(histogram.GetHistogramType())
    header += "\n\t\tIncrement width: " + str(histogram.GetIncrementWidth())
    header += "\n\t\tCounter width: " + str(histogram.GetCounterWidth())
    header += "\n\t\tOrbit counter: " + str(histogram.GetOrbitCounter())
    header += "\n\t\tCounter overflow: " + str(histogram.GetCounterOverflow())
    header += "\n\t\tIncrement overflow: " + str(histogram.GetIncrementOverflow())
    header += "\n\t\tNumber of units: " + str(histogram.GetNunits())

    counter_data = "\n\nNon-zero counter bins:"
    counter_data += ("\n\t{0:15s} {1:15s}".format("Bin", "Counts"))
    counters = histogram.GetCounters()
    for bin_id in range(histogram.GetNbins()):
        if counters[bin_id] > 0:
            counter_data += ("\n{0:15d} {1:15d}".format(bin_id, counters[bin_id]))

    mask_errors = "\n\nUnit mask:"
    mask_errors += ("\n\t{0:15s} {1:15s}".format("Unit", "Masked"))
    for unit in histogram.GetMaskedUnits():
        mask_errors += ("\n{0:15d} {1:15d}".format(unit, 1))

    if len(histogram.GetErrors()) > 0:
        mask_errors += "\n\nErrors:"
        mask_errors += ("\n\t{0:15s} {1:15s} {2:30s}".format("Orbit", "BX", "Module list"))
        for error in histogram.GetErrors():
            mask_errors += ("\n{0:15d} {1:15d}".format(error.GetOrbitCounter(), error.GetBxCounter()))
            mask_errors += "\t\t\t"
            for unit in error.GetUnitList():
                mask_errors += str(unit)
                mask_errors += " "
    if doPrint:
        print(header+counter_data+mask_errors+"\n\n")
    return(header+counter_data+mask_errors+"\n\n")

# test class
class Test:
    def __init__(self, NITERATIONS = 10, DOCONFTRIGGER = False):
        # configure
        self.histogram_reset()

        # run
        self.success = self.run(NITERATIONS, DOCONFTRIGGER)

    def histogram_reset(self):
        board.write("lumi.csr.conf.manual_oc0", 0)
        board.write("lumi.csr.conf.manual_oc0", 1)

    def read_fifo(self):
        timeout = 10  # in sec
        single_wait = 0.01  # in sec
        timeout_in_counts = timeout / single_wait

        # wait for the fifo to become non-empty
        state = board.read("lumi.bril.fifo_stat")
        fifo_empty = ((state >> 31) & 0x1)
        fifo_valid = ((state >> 30) & 0x1)
        fifo_words_cnt = ((state >> 0) & 0xfffffff)
        timeout_counter = 0
        while (fifo_empty or (not fifo_valid) or fifo_words_cnt == 0):
            if timeout_counter == timeout_in_counts:
                print("ERROR Reached fifo empty timeout, [empty,valid,words_cnt]: [", fifo_empty, ",", fifo_valid, ",",
                      fifo_words_cnt, "]")
                return None
            else:
                sleep(single_wait)
                state = board.read("lumi.bril.fifo_stat")
                fifo_empty = ((state >> 31) & 0x1)
                fifo_valid = ((state >> 30) & 0x1)
                fifo_words_cnt = ((state >> 0) & 0xfffffff)
                timeout_counter += 1

        # now read one word and get the data size
        data = [board.read("lumi.bril.fifo_data")]
        if ((data[0] >> 24) & 0xff) != 0xff:
            print("ERROR header data is corrupted: ", hex(data[0]))
            print("State [empty,valid,words_cnt]: [", fifo_empty, ",", fifo_valid, ",", fifo_words_cnt, "]")
            return None
        total_number_of_words = (data[0] >> 0) & 0xFFFF

        # now wait for the fifo to get enough data
        fifo_words_cnt = board.read("lumi.bril.fifo_stat.words_cnt")
        timeout_counter = 0
        while (fifo_words_cnt < total_number_of_words - 1):
            if timeout_counter == timeout_in_counts:
                print("Reach timeout waiting for the full package, Current words counter: ", fifo_words_cnt)
                return None
            else:
                sleep(single_wait)
                fifo_words_cnt = board.read("lumi.bril.fifo_stat.words_cnt")
                timeout_counter += 1

        # now read the data finally
        data.extend(board.readBlock("lumi.bril.fifo_data", total_number_of_words - 1))

        # return
        return data

    def data_readout_worker(self):
        while self.fRunning:
            # read fifo
            fifo_data = self.read_fifo()

            # check for error
            if fifo_data == None:
                print("No data received, timeout")
                self.fDataQueue.put(None)
                break
            else:
                self.fDataQueue.put(fifo_data)

    def run(self, NITERATIONS, DOCONFTRIGGER = False):

        # start data readout thread
        self.fRunning = True
        self.fDataQueue = queue.Queue()
        ReadoutThread = threading.Thread(target=self.data_readout_worker)
        ReadoutThread.daemon = True
        ReadoutThread.start()

        # start triggers
        if DOCONFTRIGGER:
            board.write("trigger.conf.ttgen.test_tt_bx", 128)
            board.write("trigger.conf.ttgen.test_tt_orbit_prescale", 7)
            board.write("trigger.conf.ttgen.test_tt_periodic_enable", 1)
            board.write("trigger.conf.ttgen.test_tt_enable", 1)

        # time
        start_time = time.time()

        iteration_counter = 0
        fail_counter = 0
        counter_overflow_counter = 0
        expected_lumi_section = -1
        expected_lumi_nibble = -1

        while True:
            # also do termination condition
            if self.fRunning and (iteration_counter == NITERATIONS):
                self.fRunning = False
                ReadoutThread.join()
            elif self.fRunning == False:  # readout thread already joined, put stop signatura in the data
                self.fDataQueue.put(None)

            # process data
            fifo_data = self.fDataQueue.get()
            if fifo_data is None:
                break
            else:
                # parse header
                fpga_histogram = BrilHistogram(fifo_data)

                # now compare
                if fpga_histogram != None:
                    counter_overflow_counter += fpga_histogram.GetCounterOverflow()
                    hist_printout = histogram_str(fpga_histogram, False)
                    histo_file = open("./stored_histograms/histo" + "_run_%d"%fpga_histogram.GetCmsRun() + "_section_%d"%fpga_histogram.GetLumiSection() + "_nibble_%d"%fpga_histogram.GetLumiNibble() + ".txt", "w")  
                    histo_file.write(hist_printout)
                    histo_file.close()
                    current_lumi_section = fpga_histogram.GetLumiSection()
                    current_lumi_nibble = fpga_histogram.GetLumiNibble()
                    if expected_lumi_section+expected_lumi_nibble > 0:
                        if current_lumi_section != expected_lumi_section or current_lumi_nibble != expected_lumi_nibble:
                            print("Missed a histogram probably - wrong lumi nibble section")
                            fail_counter += 1
                    expected_lumi_section = current_lumi_section+1 if (current_lumi_nibble%64 == 0) else current_lumi_section
                    expected_lumi_nibble = current_lumi_nibble%64 + 1 # because it's 1to64 not 0to63
                else:
                    fail_counter += 1

                if (iteration_counter % (0.05*NITERATIONS) == 0):
                    print("Iteration #", iteration_counter)

                # task done
                iteration_counter += 1
                self.fDataQueue.task_done()

        # thatit
        # self.fDataQueue.join()

        # done
        total_time = time.time() - start_time
        print("Total time: ", total_time, " seconds, per iteration: ", total_time / iteration_counter, " seconds")
        print("Number of fails: ", fail_counter)
        print("Number of overflow: ", counter_overflow_counter)

        # calculate return value
        if fail_counter > 0:
            return -1
        # return otherwise fine
        return 0
