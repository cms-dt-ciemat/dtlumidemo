#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# some auxiliary functions to automate ipbus access for read/write
#

import uhal

def read(device, reg, mask = 0xFFFFFFFF, nodispatch = False):
  """Versatile ipbus read: depending on "reg" nature, a read by name (reg is textual) or by address [and mask] is performed."""
  if type(reg) in [int,str]:
    tmp = device.getClient().read(reg, mask) if isinstance(reg,int) else device.getNode(reg).read()
    if nodispatch: return tmp
    else:
      device.dispatch()
      return int(tmp)

  elif isinstance(reg,list):
    tmp = [read(device, r, nodispatch = True) for r in reg]
    if nodispatch: return tmp
    else:
      device.dispatch()
      return map(int,tmp)

  else: raise Exception('"reg" must be an integer address or a string register or a list of integer/strings')

def write_name(device, regname, data):
  """Writes "data" in register "regname" in "device", then dispatches."""
  device.getNode(regname).write(data)
  device.dispatch()

def write_addr(device,addr,data,mask = 0xFFFFFFFF):
  """Writes 32-bit "data" word at address "addr" in "device", with optional "mask", then dispatches."""
  device.getClient().write(addr,data, mask)
  device.dispatch()

def write(device, reg, data, mask = 0xFFFFFFFF):
  """Versatile ipbus write: depending on "reg" nature, a write by name (reg is textual) or by address [and mask] is performed."""
  if isinstance(reg,int): return write_addr(device, reg, data, mask)
  elif isinstance(reg,str): return write_name(device, reg, data)
  else: raise Exception('"reg" must be an integer address or a string register')

def singlebitaction(device, regname):
  """
  Writes to the address "regname" a data word equal to its mask, then dispatches.
  This is done in order to prevent initial read of the register and re-write of the same data read with only the incumbent bits modified.
  """
  addr = device.getNode(regname).getAddress()
  mask = device.getNode(regname).getMask()
  write(device,addr,data = mask)

def reg_num2name(device, regnum, mask = 0xFFFFFFFF):
  allnodes = device.getNodes()
  finalnodes = [node for node in allnodes if not any([(len(node2)>len(node) and node2.find(node) == 0 and node2[len(node)]=='.') for node2 in allnodes])]
  tmp = [regname for regname in finalnodes if ( regnum == device.getNode(regname).getAddress() and mask&device.getNode(regname).getMask() )]
  #tmp2 = [regname for regname in tmp if not any([(regname in regname2 and len(regname2)>len(regname)) for regname2 in tmp])]
  return tmp

def regs_dump(device, node):
  """Returns a dict containing the dump of the readable registers in device under 'node'."""
  if device.getNode(node).getNodes():
    children = {}
    for child in device.getNode(node).getNodes():
      if '.' not in child:
        children[child] = regs_dump(device,node+'.'+child)
    return children
  return read_name(device,node) if device.getNode(node).getPermission()!=uhal.NodePermission.WRITE else 'not readable'
  
def conf_compare(conf1, conf2):
  """Compares two configuration dump dictionaries and prints the differences between them."""
  return _conf_compare(conf1, conf2, '')

def _conf_compare(c1, c2, pre):
  for node in c1:
    if isinstance(c1[node],dict):
      _conf_compare(c1[node],c2[node],pre+'.'+node)
    else:
      if c1[node]!=c2[node]:
        print( pre+'.'+node, hex(c1[node]),hex(c2[node]))
